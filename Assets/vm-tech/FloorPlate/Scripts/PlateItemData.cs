﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class PlateItemData 
{
//	[SerializeField]
//	public Sprite sprite;
//
//	[SerializeField]
//	public string label;
//
//	[SerializeField]
//	public Color tint;

	[SerializeField]
	public GameObject prefab;
}

[Serializable]
public class PlateItemDataSet : ScriptableObject
{
	[SerializeField]
	public PlateItemData[] itemData;
}