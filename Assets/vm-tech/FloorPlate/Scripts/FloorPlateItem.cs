﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System;

public class FloorPlateItem : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler 
{
	public Transform icon;
	private Image sprite;
	private Text text;
	
	//private PlateItemData itemData;

	float targetRadius;
	float targetAngle;
//	float targetTilt;
	float width;

	void Awake()
	{
		findComponents();
	}

	void findComponents ()
	{
		icon = icon ?? transform.GetChild(0);
		sprite = sprite ?? GetComponentInChildren<Image>();
		text = text ?? GetComponentInChildren<Text>();
	}

	public void Setup(PlateItemData data, float w, float r, float rot, Transform head)
	{
		//itemData = data;

		width = w;
		targetAngle = rot;
		targetRadius = r;
//		targetTilt = tilt;

		findComponents();

		Vector3 direction = icon.position - head.position;
		icon.LookAt (icon.position + direction);

		sprite.GetComponent<RectTransform>().sizeDelta = new Vector2(width, width);

		Refresh();
	}

	public void Refresh ()
	{
//		sprite.sprite = itemData.sprite;
//		sprite.color = itemData.tint;
//
//		text.text = itemData.label;

		icon.localPosition = new Vector3(0f, 0f, targetRadius);
//		icon.localRotation = Quaternion.Euler(new Vector3(targetTilt, 0f, 0f));
		transform.localRotation = Quaternion.Euler(new Vector3(0f, targetAngle, 0f));
	}
	
	public void OnPointerEnter (PointerEventData eventData)
	{
//		throw new NotImplementedException("FloorPlateItem.OnPointerEnter");
	}
	
	public void OnPointerExit (PointerEventData eventData)
	{
//		throw new NotImplementedException("FloorPlateItem.OnPointerExit");
	}
}
