﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System.Collections.Generic;

using DG.Tweening;

public class FloorPlate : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
	public GameObject FloorPlateItemPrefab;
	public float radius;
//	public float itemTilt;
	[SerializeField]
	public PlateItemData[] itemData;

	private Transform head;
	private CanvasGroup canvasGroup;
	private Transform itemsTransform;

	private Vector3 initialDir;
	private Vector3 dir;

	private List<FloorPlateItem> items;

	private bool lookingAt = false;


	//private float MAX_ARC = 60f;
	private float ITEM_WIDTH_UNITS = 64f;
	private float ITEM_ARC_DEGREES = 15f;
	private float MAX_SPACING = 1f;

	private float rightBounds;
	//private float leftBounds;
	private float arcWidth;
	private float arcUpperBounds;
	private float arcLowerBounds;

	private float arcCurrentBounds;


	void Awake()
	{
		findComponents();
		canvasGroup.alpha = 0f;

		float len = itemData.Length;

		ITEM_ARC_DEGREES = Mathf.Atan2(ITEM_WIDTH_UNITS / 2f, radius) * 2f * Mathf.Rad2Deg;

		MAX_SPACING = ITEM_ARC_DEGREES * 0.1f;


		arcWidth = ITEM_ARC_DEGREES * (len - 1f) + MAX_SPACING * (len - 1f);

		rightBounds = arcWidth / 2f;
		//leftBounds = -rightBounds;
		float startAngle = -arcWidth / 2f;

		float arcInterval = 0f;
		if(len > 1)
			arcInterval = arcWidth / (len - 1);

		for(int i = 0; i < len; i++)
		{
			float angle = startAngle + i * arcInterval;

			PlateItemData d = itemData[i];
			FloorPlateItem item = GameObject.Instantiate<GameObject>(d.prefab).GetComponent<FloorPlateItem>();
			item.transform.SetParent(itemsTransform, false);
			item.Setup(d, ITEM_WIDTH_UNITS, radius, angle, head);
			items.Add(item);
		}

		/// calculate upper/lower bounds for looking down

		Vector3 localUpperPoint = itemsTransform.localPosition + new Vector3(0f, ITEM_WIDTH_UNITS * 2f, radius);
//		float lossyScale = itemsTransform.lossyScale.x;
//		Vector3 localUpperPoint = 
//			itemsTransform.localPosition;
//			itemsTransform.TransformDirection(Vector3.up) * lossyScale * ITEM_WIDTH_UNITS + 
//			itemsTransform.TransformDirection(Vector3.up) * new Vector3(0f, ITEM_WIDTH_UNITS, radius);

		Vector3 worldUpperPoint = transform.TransformPoint(localUpperPoint);

		Debug.DrawLine(head.position, worldUpperPoint, Color.red, 1000f);

		Vector3 delta = head.position - worldUpperPoint;
		Vector3 flat = delta;
		flat.y = 0;

		arcUpperBounds = AngleSigned(flat, delta, Vector3.right);
		arcLowerBounds = 70f;
		arcCurrentBounds = arcLowerBounds;

	}

	void Start () 
	{
		RefreshItems();
	}

	void findComponents ()
	{
		
		head = FindObjectOfType<CardboardHead>().transform;

		items = items ?? new List<FloorPlateItem>();

		itemsTransform = transform.GetChild(0);

		canvasGroup = GetComponentInChildren<CanvasGroup>();
	}
	
	void Update () 
	{
		Vector3 headRot = head.eulerAngles;
		Vector3 headDir = Vector3.ProjectOnPlane(head.forward, Vector3.up);

		if(headRot.x >= arcCurrentBounds && headRot.x < 90f)
		{
			if(lookingAt == false)
			{
				arcCurrentBounds = arcUpperBounds;
				lookingAt = true;

				initialDir = headDir;
				dir = initialDir;
					
				// transition IN
				canvasGroup.DOFade(1f, 0.2f);

				items.ForEach(x => x.SendMessage("OnShow", SendMessageOptions.DontRequireReceiver));
			}


			float diff = AngleSigned(initialDir, headDir, Vector3.up);
			if(Mathf.Abs(diff) > rightBounds)
			{
				if(diff >= 0)
					diff = diff - rightBounds;
				else
					diff = diff + rightBounds;
				
				initialDir = Quaternion.AngleAxis(diff, Vector3.up) * initialDir;
				dir = initialDir;
			}

			transform.forward = dir;
		}
		else
		{
			if(lookingAt == true)
			{
				lookingAt = false;

				arcCurrentBounds = arcLowerBounds;

				// transition OUT
				canvasGroup.DOFade(0f, 0.2f);

				items.ForEach(x => x.SendMessage("OnHide", SendMessageOptions.DontRequireReceiver));
			}

			dir = headDir;
		}
	}

	public static float AngleSigned(Vector3 v1, Vector3 v2, Vector3 n)
	{
		return Mathf.Atan2(
			Vector3.Dot(n, Vector3.Cross(v1, v2)),
			Vector3.Dot(v1, v2)) * Mathf.Rad2Deg;
	}

	public void RefreshItems ()
	{

	}

	public void OnPointerEnter (PointerEventData eventData)
	{
//		canvasGroup.alpha = 1f;
////		follow = false;
	}

	public void OnPointerExit (PointerEventData eventData)
	{
//		canvasGroup.alpha = 0f;
//		follow = true;
	}
}
