﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;

public class SimpleLog : MonoBehaviour {

	private static SimpleLog _instance= null;

	private List<string> messages;

	const int MAX_ENTRIES = 8;
	private Canvas canvas;

	Text textfield;
    private bool shown = false;

    void Awake() {
		_instance = this;

		textfield = GetComponent<Text>();

		canvas = GameObject.Find("SimpleLog").GetComponent<Canvas>();

		messages = new List<string>();



		shown = true;

        Refresh();
	}

    void Update() {
        if (Input.GetKeyDown(KeyCode.BackQuote))
        {
            ToggleConsole();
        }
    }

    private void Refresh()
    {
        canvas.enabled = shown;
    }

    private void ToggleConsole()
    {
        shown = !shown;
    }

    public static void Log (params string[] args) {
		_instance._log(args);
	}

	private void _log(params string[] args) {
		string message = string.Join(" ", args);
//		messages.(message);
		messages.Add(message);

		if(messages.Count > MAX_ENTRIES) {
			messages.RemoveAt(0);
		}

		textfield.text = string.Join("\n", messages.ToArray());
	}
}


public static class logger {
	public static void log(this UnityEngine.Object target, params object[] args) {
		Debug.logger.Log(target.ToString(), string.Join(" ", args.Select(a => (a ?? "").ToString()).Cast<string>().ToArray()), target);
	}
}