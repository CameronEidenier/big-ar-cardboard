﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

using DG.Tweening;

public class ReticleScript : MonoBehaviour {
	
	public static ReticleScript instance;
	[SerializeField] private Sprite spriteDefault;
	[SerializeField] private Sprite spriteActive;

	public Image normal;
	public Image hover;
	
	private int lookedCount = 0;
	private bool on = false;
	
	// little one is 0.58 of the big one.
	private float BIG_TO_LITTLE = 0.58f;

	void Awake()
	{
		instance = this;
	}

	void Start()
	{
		//		setAlpha(normal, 1f);
		//		setAlpha(hover, 0f);
		
		normal.DOFade(1f, 0f);
		hover.DOFade(0f, 0f);
		hover.transform.localScale = Vector3.one * BIG_TO_LITTLE;
		
		
		normal.sprite = spriteDefault;
		hover.sprite = spriteActive;
	}

	void setAlpha (Image image, float s)
	{
		Color c = image.color;
		c.a = s;
		image.color = c;
	}
	
	void Refresh ()
	{
		lookedCount = Mathf.Clamp(lookedCount, 0, int.MaxValue);
		
		if(lookedCount <= 0 && on) 
		{
			on = false;
			//			normal.sprite = spriteDefault;
			
			DOTween.Kill(normal);
			DOTween.Kill(hover);
			DOTween.Kill(hover.transform);
			
			normal.DOFade(1f, 0.1f).SetDelay(0.1f);
			hover.DOFade(0f, 0.1f).SetDelay(0.1f);
			hover.transform.DOScale(BIG_TO_LITTLE, 0.1f).SetEase(DG.Tweening.Ease.InQuad);
		}
		else if(lookedCount > 0 && !on)
		{
			on = true;
			//	        normal.sprite = spriteActive;
			
			DOTween.Kill(normal);
			DOTween.Kill(hover);
			DOTween.Kill(hover.transform);
			
			normal.DOFade(0f, 0.1f);
			hover.DOFade(1f, 0.1f);
			hover.transform.DOScale(1f, 0.4f).SetEase(DG.Tweening.Ease.OutBack);
		}
	}
	
	public static void ReticleDefault ()
	{
		if(instance != null)
			instance.reticleDefault();
	}
	
	public static void ReticleActive ()
	{
		if(instance != null)
			instance.reticleActive();
	}
	
	private void reticleDefault ()
	{
		lookedCount--;
		Refresh();
	}
	
	private void reticleActive ()
	{
		lookedCount++;
		Refresh();
	}
}
