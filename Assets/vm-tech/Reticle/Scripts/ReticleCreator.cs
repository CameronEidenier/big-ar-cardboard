﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;

public class ReticleGUI : MonoBehaviour {

	[SerializeField] private GameObject reticlePrefab;

	private GameObject reticleGObject;

	void Awake() {
		reticleGObject = GameObject.Instantiate(reticlePrefab) as GameObject;
	}

	void Start() 
	{
		CardboardHead head = FindObjectOfType<CardboardHead>();
		
		reticleGObject.transform.SetParent(head.transform);
		reticleGObject.transform.localRotation = Quaternion.identity;
		reticleGObject.transform.localPosition = Vector3.zero;
		reticleGObject.transform.localScale = Vector3.one;
	}

	void OnDestroy()
	{
		if(reticleGObject != null)
		{
			reticleGObject.transform.SetParent(null);
			Destroy(reticleGObject);
		}
	}
}
