﻿// Created by Kay
// Copyright 2013 by SCIO System-Consulting GmbH & Co. KG. All rights reserved.

// Modified by Dill0wn
// author's repo: https://github.com/kayy/BundleVersionChecker 
using UnityEngine;
using UnityEditor;
using System.IO;
using System;
using System.Reflection;
using System.Linq;
using System.Collections.Generic;

[InitializeOnLoad]
public class BundleVersionChecker
{
	interface IPropertyGenerator {

		Type FieldType {get;}
		string FieldName {get;}
		string StringFormattedValue {get;}

		bool Unequal();
	}

	class PropertyGenerator<TClass, T> :IPropertyGenerator {

		public string FieldName { get; protected set; }
		public T DesiredValue { get; protected set; }

		public Type FieldType { get; protected set; }
		public Type ClassType { get; protected set; }


		public object ExistingValue {
			get {
				FieldInfo field = ClassType.GetField(FieldName);
				return field.GetValue(null);				
			}
		}

		public string StringFormattedValue {
			get { 
				if(DesiredValue is string)
					return "\"" +DesiredValue.ToString() + "\""; 
				else
					return DesiredValue.ToString();
			}
		}

		public PropertyGenerator(string fieldName, T desiredValue) {
			FieldType = typeof(T);
			ClassType = typeof(TClass);

			this.FieldName = fieldName;
			this.DesiredValue = desiredValue;
		}

		public bool Unequal() {

			try {
				return ExistingValue.Equals(DesiredValue) == false;
//				Type genericType = typeof(VersionProperty<,>).MakeGenericType(new Type[] { versionType,  field.FieldType } );
//				ConstructorInfo ctor = genericType.GetConstructor(new Type[] { typeof(string),  field.FieldType } );
//				var prop = ctor.Invoke(new object[] { "bundleVersion", PlayerSettings.bundleVersion} );
			}
			catch(Exception e) {
				Debug.Log("Error while trying to read previous values. Probably a mismatch");
				return true;
			}
		}

		public override string ToString ()
		{
			return string.Format ("[PropertyGenerator: FieldName={0}, DesiredValue={1}, FieldType={2}, ClassType={3}, ExistingValue={4}, StringFormattedValue={5}]", FieldName, DesiredValue, FieldType, ClassType, ExistingValue, StringFormattedValue);
		}

	}


	/// <summary>
	/// Class name to use when referencing from code.
	/// </summary>
	static string ClassName { get; set; }

	static string TargetCodeFile { get { return "Assets/Standard Assets/" + ClassName + ".cs"; } }

	static List<IPropertyGenerator> properties;

	static BundleVersionChecker () {

		ClassName = typeof(CurrentBundleVersion).Name;

		properties = new List<IPropertyGenerator>();
		properties.Add(new PropertyGenerator<CurrentBundleVersion, string>("bundleVersion", PlayerSettings.bundleVersion));
		properties.Add(new PropertyGenerator<CurrentBundleVersion, string>("iOSbuildNumber", PlayerSettings.iOS.buildNumber));
		properties.Add(new PropertyGenerator<CurrentBundleVersion, int>("AndroidBundleVersionCode", PlayerSettings.Android.bundleVersionCode));


		CheckAgainstExisting();
	}

	static void CheckAgainstExisting() {

		if(properties.Any(x => x.Unequal())) {
			string output = string.Format("[{0}] Is out of date. Updating file \"{1}\".\n", ClassName, TargetCodeFile);
			foreach(IPropertyGenerator prop in properties) {
				output += prop.ToString() + "\n";
			}

			Debug.Log(output);

//			Debug.Log (	string.Format("Updating CurrentBundleVersion, Writing to file \"{2}\".\nFROM: {0}. TO: {1}", 
//				FormatVersionOutput(lastBundleVersion, lastShortBundleVersion, lastBundleVersionCode), 
//				FormatVersionOutput(bundleVersion, shortBundlVersion, bundleVersionCode), 
//				TargetCodeFile
//			));
			CreateNewBuildVersionClassFile ();
		}
	}

	static string FormatVersionOutput(string bundleVersion, string shortBundlVersion, int bundleVersionCode)
	{
		return string.Format ("{{bundleVersion: {0}, shortBundlVersion: {1}, bundleVersionCode: {2}}}", bundleVersion, shortBundlVersion, bundleVersionCode);
	}

	static string CreateNewBuildVersionClassFile () {
		using (StreamWriter writer = new StreamWriter (TargetCodeFile, false)) {
			try {
				string code = GenerateCode ();
				writer.WriteLine ("{0}", code);
			} catch (System.Exception ex) {
				string msg = " threw:\n" + ex.ToString ();
				Debug.LogError (msg);
				EditorUtility.DisplayDialog ("Error when trying to regenrate class", msg, "OK");
			}
		}
		return TargetCodeFile;
	}


	/// <summary>
	/// Regenerates (and replaces) the code for ClassName with new bundle version id.
	/// </summary>
	/// <returns>
	/// Code to write to file.
	/// </returns>
	/// <param name='bundleVersion'>
	/// New bundle version.
	/// </param>

	static string GenerateCode () {
		string code = "public class " + ClassName + "\n{\n";

		foreach(IPropertyGenerator prop in properties) {
			code += System.String.Format ("\tpublic static readonly {0} {1} = {2};\n", prop.FieldType, prop.FieldName, prop.StringFormattedValue);
		}

//		code += System.String.Format ("\tpublic static readonly string bundleVersion = \"{0}\";\n", PlayerSettings.bundleVersion);
//		code += System.String.Format ("\tpublic static readonly string shortBundleVersion = \"{0}\";\n", PlayerSettings.shortBundleVersion);
//		code += System.String.Format ("\tpublic static readonly int bundleVersionCode = {0};", PlayerSettings.Android.bundleVersionCode);

		code += "\n}\n";
		return code;
	}

}