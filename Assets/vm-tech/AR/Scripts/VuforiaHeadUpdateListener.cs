﻿using UnityEngine;
using System.Collections;

public class VuforiaHeadUpdateListener : MonoBehaviour {
	bool ARMode = false;

	void OnEnable () {
		FindObjectOfType<CardboardHead>().OnHeadUpdated += OnCardboardHeadUpdated;
		//FindObjectOfType<ARController>().OnARModeChanged += OnARModeChanged;
	}

	void OnDisable() {
		CardboardHead ch = FindObjectOfType<CardboardHead>();
		if(ch != null)
			ch.OnHeadUpdated -= OnCardboardHeadUpdated;
		
		//ARController ar = FindObjectOfType<ARController>();
		//if(ar != null)
			//ar.OnARModeChanged -= OnARModeChanged;
	}

	void Start() {
		
	}

	void OnARModeChanged (bool ARModeEnabled)
	{
		this.ARMode = ARModeEnabled;
	}

	void OnCardboardHeadUpdated(GameObject gameObject) {
        if (Vuforia.VuforiaBehaviour.Instance != null && Vuforia.VuforiaBehaviour.Instance.enabled == true)

        //		if(ARController.ARModeEnabled == true)
        //if(ARMode == true)
        {

            Debug.Log("Updating Vuforia State");
			Vuforia.VuforiaBehaviour.Instance.UpdateState (false, true);
		}
	}
}
