﻿using UnityEngine;
using System.Collections;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization.Formatters;
using System.IO;
using System.Security.Cryptography;
using System.Text;


public class AbstractBlob : MonoBehaviour
{
	private string BLOB_FILE = "VMKit.blob.dat";
	private string BLOB_PATH { get { return BuildPath(BLOB_FILE); } }

	private string PLAYER_PREFS_KEY = "VMKit.blob";
	private string BuildPath(string fileName) { return Path.Combine(Application.persistentDataPath, fileName); }

	protected bool dirty = false;

	private DESCryptoServiceProvider des;
	private byte[] key;
	private byte[] iv;

	private object _data;

	protected object GetData ()
	{
		return _data;
	}

	protected void Setup(object o)
	{
		_data = o;

		key = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };
		iv = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };

		des = new DESCryptoServiceProvider();
		des.Key = key;
		des.IV = iv;
		des.Padding = PaddingMode.PKCS7;
		des.Mode = CipherMode.CBC;

//		// alternative method?
//		string PW_HASH = "password";
//		string SALT_KEY = "1234567890";
//		string VI_KEY = "0987654321";
//
//		// encrypt
//		byte[] keyBytes = new Rfc2898DeriveBytes(PW_HASH, Encoding.ASCII.GetBytes(SALT_KEY)).GetBytes(256/8);
//		var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.Zeros };
//		var encryptor = symmetricKey.CreateEncryptor(keyBytes, Encoding.ASCII.GetBytes(VI_KEY));
//
//		// decrypt
//		byte[] keyBytes = new Rfc2898DeriveBytes(PasswordHash, Encoding.ASCII.GetBytes(SaltKey)).GetBytes(256 / 8);
//		var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.None };
//		var decryptor = symmetricKey.CreateDecryptor(keyBytes, Encoding.ASCII.GetBytes(VIKey));

		Load();
	}

	protected void Clear() {

	}

	protected void CreateDataObject() {
		
	}

	void Update()
	{
		if(dirty == true)
			Save();
	}

//	void SaveAll()
//	{
//		if(dirty == false)
//			return;
//
//		dirty = false;
//		Save();
//	}
//
//	private void LoadAll()
//	{
//		if(dirty == true)
//			SaveAll();
//		
//		Load();
//	}

	/// <summary>
	/// Saves all encrypted a2.
	/// </summary>
	protected void Save()
	{
		if(dirty == false)
			return;

		dirty = false;

		string rawJSON = JsonUtility.ToJson(_data);
		Debug.Log("Save JSON:\n" + rawJSON);

		byte[] plainTextBytes = Encoding.UTF8.GetBytes(rawJSON);
		byte[] cipherTextBytes;

		using (MemoryStream ms = new MemoryStream())
		using (CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(), CryptoStreamMode.Write))
		{
			cs.Write(plainTextBytes, 0, plainTextBytes.Length);
			cs.FlushFinalBlock();
			cipherTextBytes = ms.ToArray();
		}

		string encryptedJSON = Convert.ToBase64String(cipherTextBytes);

//		using (FileStream fs = new FileStream(BLOB_PATH, FileMode.OpenOrCreate, FileAccess.Write))
//		{
//			StreamWriter sw = new StreamWriter(fs);
//			sw.Write(encryptedJSON);
//			sw.Flush();
//			sw.Close();
//		}

		PlayerPrefs.SetString(PLAYER_PREFS_KEY, encryptedJSON);
	}

	protected void Load ()
	{

		if(dirty == true)
			Save();
		
//		if(File.Exists(BLOB_PATH) == false)
//			SaveAll();
//		
//		using (var fs = new FileStream(BLOB_PATH, FileMode.Open, FileAccess.Read))
//		{
//			StreamReader sr = new StreamReader(cryptoStream);
//			string json = sr.ReadToEnd();
//			Debug.Log("Loading Blob:\n" + json);
//			JsonUtility.FromJsonOverwrite(json, blob);
//		}

		else if(PlayerPrefs.HasKey(PLAYER_PREFS_KEY) == false)
		{
			Save();
			return;
		}

		string encryptedJSON;

//		using (FileStream fs = new FileStream(BLOB_PATH, FileMode.Open, FileAccess.Read))
//		{
//			StreamReader sw = new StreamReader(fs);
//			encryptedJSON = sw.ReadToEnd();
//		}

		encryptedJSON = PlayerPrefs.GetString(PLAYER_PREFS_KEY);

		byte[] cipherTextBytes = Convert.FromBase64String(encryptedJSON);
		byte[] plainTextBytes = new byte[cipherTextBytes.Length];

		int decryptedByteCount;

		using (MemoryStream ms = new MemoryStream(cipherTextBytes))
		using (CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(), CryptoStreamMode.Read))
		{
			decryptedByteCount = cs.Read(plainTextBytes, 0, plainTextBytes.Length);
		}

		string rawJSON = Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount).TrimEnd("\0".ToCharArray());
		Debug.Log("Loaded JSON:\n" + rawJSON);
		JsonUtility.FromJsonOverwrite(rawJSON, _data);
	}
}














//private void SaveAllOriginal()
//{
//	BinaryFormatter Bi = new BinaryFormatter();
//	FileStream file = File.Create(BLOB_PATH);
//	Bi.Serialize(file, blob);
//	file.Close();
//}
//
//void LoadAllOriginal()
//{
//	BinaryFormatter binaryFormatter = new BinaryFormatter();
//	FileStream file = File.Open(BLOB_PATH, FileMode.OpenOrCreate);
//
//	try 
//	{
//		blob = (SaveBlob)binaryFormatter.Deserialize(file);
//	} 
//	catch 
//	{
//		SetupTheBlob();
//	} 
//	finally 
//	{
//		file.Close();
//	}
//}




//	/// <summary>
//	/// Saves all encrypted a.
//	/// </summary>
//	private void SaveAllEncryptedA()
//	{
//		using (FileStream fs = new FileStream(BLOB_PATH, FileMode.OpenOrCreate, FileAccess.Write))
//		using (CryptoStream cryptoStream = new CryptoStream(fs, des.CreateEncryptor(), CryptoStreamMode.Write))
//		{
//			BinaryFormatter binaryFormatter = new BinaryFormatter();
//			binaryFormatter.Serialize(cryptoStream, blob);
//
//			//cryptoStream.FlushFinalBlock();
//		}
//	}
//
//	void LoadAllEncryptedA ()
//	{
//		blob = new SaveBlob();
//
//		if(File.Exists(BLOB_PATH) == false)
//		{
//			SaveAll();
//		}
//
//		using (var fs = new FileStream(BLOB_PATH, FileMode.Open, FileAccess.Read))
//		using (var cryptoStream = new CryptoStream(fs, des.CreateDecryptor(), CryptoStreamMode.Read))
//		{
//			BinaryFormatter binaryFormatter = new BinaryFormatter();
//			blob = (SaveBlob)binaryFormatter.Deserialize(cryptoStream);
//		}
//	}




//	/// <summary>
//	/// fs/ ms going straight into and out of JSON
//	/// </summary>
//	private void SaveAllEncryptedB()
//	{
//		using (FileStream fs = new FileStream(BLOB_PATH, FileMode.OpenOrCreate, FileAccess.Write))
//		using (CryptoStream cryptoStream = new CryptoStream(fs, des.CreateEncryptor(), CryptoStreamMode.Write))
//		{
//			StreamWriter sw = new StreamWriter(cryptoStream);
//			string json = JsonUtility.ToJson(blob, true);
//			Debug.Log("Saving Blob:\n" + json);
//			sw.Write(json);
//		}
//	}
//	
//	private void LoadAllEncryptedB()
//	{
//		blob = new SaveBlob();
//
//		if(File.Exists(BLOB_PATH) == false)
//		{
//			SaveAll();
//		}
//
//		using (var fs = new FileStream(BLOB_PATH, FileMode.Open, FileAccess.Read))
//		using (var cryptoStream = new CryptoStream(fs, des.CreateDecryptor(), CryptoStreamMode.Read))
//		{
//			StreamReader sr = new StreamReader(cryptoStream);
//			string json = sr.ReadToEnd();
//			Debug.Log("Loading Blob:\n" + json);
//			JsonUtility.FromJsonOverwrite(json, blob);
//		}
//	}