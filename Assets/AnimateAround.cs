﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class AnimateAround : MonoBehaviour {
    public bool exit = false;
    bool MovingToExit = false;
    public Vector3 exitPoint;
	void Start () {
        print("I'm Alive");
        MoveToPoint1();
        /*Sequence mySeq = DOTween.Sequence();
        mySeq.Append(transform.DOMove(new Vector3(50, 0, 0),5f));
        mySeq.Append(transform.DOMove(new Vector3(0 , 0, 50), 5f));
        mySeq.Append(transform.DOMove(new Vector3(-50, 0, 0), 5f));
        mySeq.Append(transform.DOMove(new Vector3(0, 0, -50), 5f));
        mySeq.Play();*/
    }
	
	// Update is called once per frame
	void Update () {
        if (exit) ObjectExit();
        else if (transform.position.x == 1000f && transform.position.z == 1000f) MoveToPoint2();
        else if (transform.position.x == 1000f && transform.position.z == -1000f) MoveToPoint3();
        else if (transform.position.x == -1000f && transform.position.z == -1000f) MoveToPoint4();
        else if (transform.position.x == -1000f && transform.position.z == 1000f) MoveToPoint1();

        
    }
    void MoveToPoint1()
    {
        print("Run Point 1");
        transform.DOMove(new Vector3(1000f,Random.Range(-1000f,1000f), 1000f), 2f,true).SetEase(Ease.InOutCubic);
    }
    void MoveToPoint2()
    {
        print("Run Point 2");
        transform.DOMove(new Vector3(1000f, Random.Range(-1000f, 1000f), -1000f), 2f, true).SetEase(Ease.InOutCubic);
    }
    void MoveToPoint3()
    {
        print("Run Point 3");
        transform.DOMove(new Vector3(-1000f, Random.Range(-1000f, 1000f), -1000f), 2f, true).SetEase(Ease.InOutCubic);
    }
    void MoveToPoint4()
    {
        print("Run Point 4");
        transform.DOMove(new Vector3(-1000f, Random.Range(-1000f, 1000f), 1000f), 2f, true).SetEase(Ease.InOutCubic);
    }
    void ObjectExit()
    {
        if (MovingToExit == false)
        {
            //DOTween.KillAll(true);
            //DOTween.KillAll(false);
            transform.DOMove(exitPoint, 2f, true).SetEase(Ease.InOutCubic);
            MovingToExit = true;

            if (!IsInvoking("destroySelf"))
            {
                Invoke("destroySelf", 2f);
            }
        }
    }
    void destroySelf()
    {
        Destroy(gameObject);
    }
}
