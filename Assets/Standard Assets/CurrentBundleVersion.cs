public class CurrentBundleVersion
{
	public static readonly System.String bundleVersion = "1.0.0";
	public static readonly System.String iOSbuildNumber = "2";
	public static readonly System.Int32 AndroidBundleVersionCode = 1;

}

