﻿using UnityEngine;
using System.Collections;

public class CreateObject : MonoBehaviour {
    public GameObject spawnTest;
    // Use this for initialization
    GameObject[] movingObjects;
	void Start () {
	
	}	
	// Update is called once per frame
	void Update () {
        movingObjects = GameObject.FindGameObjectsWithTag("MovingObject");
    }
    public void SpawnObject()
    {
        if (movingObjects.Length > 0)
        {
            print("Exit Call");
            if (movingObjects[0].gameObject.GetComponent<AnimateAround>().exit == false)
            {
                movingObjects[0].gameObject.GetComponent<AnimateAround>().exitPoint = transform.position;
            }            
            movingObjects[0].gameObject.GetComponent<AnimateAround>().exit = true;
        }
        else
        {
            print("Spawned Object");
            GameObject newObject = Instantiate(spawnTest, transform.position, transform.rotation) as GameObject;
            newObject.transform.localScale = transform.parent.transform.localScale*transform.localScale.x;
        }
            

        
    }
}
