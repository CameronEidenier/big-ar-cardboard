﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class GlobalConfig
{
	[SerializeField]
	public Color CubeColor = Color.white;

	public string productId;

	public string appSecret;
	public string appId;

	public string googlePlayPublicKey;

	public static GlobalConfig CreateConfig(string applicationId)
	{
		GlobalConfig config = new GlobalConfig();

		string bundlePrefix = "com.yeticgi.vmtech.rgb.";


		string variant = applicationId.Replace(bundlePrefix, "");

		if(variant == "r")
		{
			config.CubeColor = Color.red;
			config.googlePlayPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnaKURZ0UEVDtBF6jkMWQgmUeE3MaLtwdKZHNLh0GbbVtopn/ViDZx2+2l/m2zcA4UrWtNcMFU0AnghTuBtJbydrKm6hC4xsxdlYmC7dcVGfctqtbe/Ew9dpz4hHM8WHxcOZI8W2PGsWLDcL6HmIv9J7jwfi/3q4kw1+CowXgB2Ahvw+6aeZm2K7VHBJ8NHsX61v6qeNQxYvVzr+f/F/ygZcZbuuqcwIjw+YqniLx3utrFUfK5apQOtM8rs9lF0A4VGfULbYKLyqkSzYSO17MyjNI8uuSFa0YKIlXvD9OIVuXXe1NdZ1alEscx/DWmwGrzJuOYtyLvrDuTEoYB7FcfwIDAQAB";
		}
		else if(variant == "g")
		{
			config.CubeColor = Color.green;
			config.googlePlayPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtLyRpkRntQi6z1lAwCyq/sTeB+4XsM5r/Cs0MxVFduY3cN9ElQQa9WV11KOa1wm4v51dOXOYfyhmMvEb4gfkNOS2vuVqVx16Rv9a9DOwSn0ruASAecijX0eMU/jWWrrr3F2oj90LE7Ln7CQnSfz8Ct5gWc5ANwdYxg8HIf3p+YrwZ0lfGdbMFwTBxjjFQVclfIALGtfu8by5MIpSdpAENIaJpW8pHNLeYh67gWKf190ZvcvRbwq8ySIFYSNVCf6GvsmJqzSCJZVKa0761Krb3t2uRUntzZpx/1WdnpsmxQDMZicmTv5Y2TAuR9KCszXJIBv3rbvoA0nosQbFUOUoTQIDAQAB";
		}
		else if(variant == "b")
		{
			config.CubeColor = Color.blue;
			config.googlePlayPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA4BRIBlTBK2Ti9/H9NTmuc38Na/uzGdGqp8Fj1b/ZDx2cZcznd1A3DAt5q7wQqWE0mY24GkHLeCuG4G/o07p/omVDYpUt0I2fVltpX8e0ZIYqgq+n4x8+XCsAVCRCyblV5ml9qITE20TsqUIzkXj3wp5X2Lwpm4dm6By7UnCMJw2KU6yXdCJq6ZIXm7KpDzzt69oIAUFelF8T0WAZXQsIbxYzojnsoWdL+iaFvp6gcOZfx17I0MfFzCftGApe53dcXAc09mZIVt8mbAmcTUVDHa6rdq9AuCBn6vyHl0TBivrW+sVdW1n1weHU9JvpF7b7yevKvGJKnzgWIqpVu8ZAiwIDAQAB";
		}

		// "com.yeticgi.vmtech.rgb.r.01"
		config.productId = applicationId + ".01";

		return config;
	}

	internal string RGBID (string prefix, string suffix)
	{
		return prefix + "." + suffix;
	}
}
