extern "C"
{
    char* babelfish_iOS_getPreferredLanguages() {
        NSString* languages = [[NSLocale preferredLanguages] componentsJoinedByString:@","];

        if (languages == NULL) {
            return NULL;
        }
        
        return strdup([languages UTF8String]);
    }
}

