//
//  EAGLView.h
//  Unity-iPhone
//
//  Created by Maksim Narchuk on 11.06.15.
//
//
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface EAGLView: UIView

@end

extern "C"
{
    void _TD_EAGL_init(const char *database, const char *key, bool *isNativeHelpZone);
    void _TouchDetected();
}
