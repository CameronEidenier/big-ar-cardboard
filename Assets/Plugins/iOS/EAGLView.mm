//
//  EAGLView.mm
//  Unity-iPhone
//
//  Created by Maksim Narchuk on 11.06.15.
//
//
#import "EAGLView.h"
#import <UIKit/UIKit.h>
#import <TouchDecode/PTEventManager.h>
#import <TouchDecode/PTTouchDecode.h>
#import <QuartzCore/QuartzCore.h>
#import <OpenGLES/EAGLDrawable.h>
// ** uncomment to enable TouchcodeUI **
// #import <TouchDecodeUI/PTDetectionZone.h>
// #import <TouchDecodeUI/PTHelpLayer.h>
#import "UnityAppController.h"


// A class extension to declare private methods
@interface EAGLView () <PTTouchDecodeNotification, PTResponder>
{
    NSString *foundCode;
    
}


@property (nonatomic, retain) PTTouchDecode *decoder;
// ** uncomment to enable TouchcodeUI **
// @property (nonatomic,retain) PTHelpLayer *helpLayer;


@end

static NSString *_sdataBaseName;
static NSString *_skeyInit;



@implementation EAGLView
+(Class)layerClass
{
    return [CAEAGLLayer class];
}



- (id) initWithFrame:(CGRect)frame
{
    if((self=[super initWithFrame:frame]))
    {
        /**
         *beginptcode
         */
        [self setMultipleTouchEnabled:YES];
        [self setExclusiveTouch:YES];
        //initEventManager
        [[PTEventManager sharedManager] activateWithView: self];
        //inittouchdecoder
        _decoder = [[PTTouchDecode alloc] initInView: self];
        //setdelegate
        [_decoder setDelegate: self];
        
        foundCode = nil;
        
        //setupcardpool
        if(![_decoder addPoolFromFile:_sdataBaseName
                             isPacked:YES AESKey:_skeyInit])
        {
            NSLog(@"Could not load Touchcode code set: %@",
                  [_decoder getLoadError]);
        }
        
        
        //addthisobjecttoeventManager
        [[PTEventManager sharedManager] addObserver:self];
        [[PTEventManager sharedManager] setEnabled:YES];
        /**
         *endptcode
         */
    }
    return self;
}

/**pt
 ReceivesafecardcodefromPTTouchDecode.
 @parammessageANSStringwhichrepresentthecodeid.
 */
-(void)didDecodeWithResult:(PTTouchDecodeResult*)result
{
    
    NSString*out=[self toStringWithResult:result];
    NSLog(@"didReceiveSafeResult args:%@", out);
    UnitySendMessage("PTResponder","didReceiveSafeResult", [out UTF8String]);
    
}
/**pt
 ReceiveunsafecardcodefromPTTouchDecode.
 @parammessageANSStringwhichrepresentthecodeid.
 */
-(void)didUnsafeDecodeWithResult:(PTTouchDecodeResult*)result
{
    
    NSString*out=[self toStringWithResult:result];
    NSLog(@"didReceiveUnsafeResult1 args:%@", out);
    UnitySendMessage("PTResponder","didReceiveUnsafeResult", [out UTF8String]);
    
}


-(void)TCActionSuccesfulDetected:(id<PTTouchCodeIObject>)touchcode
{
    if ([touchcode aState] == TouchcodeVerified && nil == foundCode) {
        foundCode = [touchcode aResult].code_name;
        NSString*out=[self toStringWithCodenum:foundCode];
        NSLog(@"didReceiveUnsafeResult2 args:%@", out);
        UnitySendMessage("PTResponder","didReceiveUnsafeResult", [out UTF8String]);
    }
}


-(NSString*)toStringWithResult:(PTTouchDecodeResult*)result
{
    NSArray* foo=[result.code_name componentsSeparatedByString:@"#"];
    if([foo count] > 1)
        return[foo objectAtIndex:1];
    else
        return result.code_name;
}


-(NSString*)toStringWithCodenum:(NSString*)codenum
{
    NSArray* foo=[codenum componentsSeparatedByString:@"#"];
    if([foo count] > 1)
        return[foo objectAtIndex:1];
    else
        return codenum;
}
/**pt
 Receivefirsttouchcodeobject.
 @paramtouchcodeAtouchcodeobjectwithtransformation.
 @noteTemporarlynotimplemented.
 @returnsABOOLValue.
 */
-(BOOL)GlobalTouchCodeCaptured:(id<PTTouchCodeIObject>)touchcode
{
    NSString* buildArguments=@"";
    NSLog(@"capturedTouchCode args:%@", buildArguments);
    UnitySendMessage("PTResponder","capturedTouchCode", [buildArguments UTF8String]);
    return TRUE;
}
/**pt
 Receivechangesintouchcodeobjects.
 @paramtouchcodeAtouchcodeobjectwithtransformation.
 @noteTemporarlynotimplemented.
 @returnsABOOLValue.
 */
-(BOOL)GlobalTouchCodeChanged:(id<PTTouchCodeIObject>)touchcode
{
    NSString* buildArguments=@"";
    //    NSLog(@"changedTouchCode args:%@", buildArguments);
    UnitySendMessage("PTResponder","changedTouchCode", [buildArguments UTF8String]);
    return TRUE;
}
/**pt
 Receivelasttouchcodeobject.
 @paramtouchcodeAtouchcodeobjectwithtransformation.
 @noteTemporarlynotimplemented.
 @returnsABOOLValue.
 */
-(BOOL)GlobalTouchCodeLost:(id<PTTouchCodeIObject>)touchcode
{
    NSString* buildArguments=@"";
    
    NSLog(@"lostTouchCode args:%@", buildArguments);
    
    UnitySendMessage("PTResponder","lostTouchCode", [buildArguments UTF8String]);
    return TRUE;
}
-(void)touchesBegan:(NSSet*)touches withEvent:(UIEvent*)event
{
    [_decoder processTouches:touches withEvent:event]; //includedbypt
    UnitySendTouchesBegin(touches,event);
    
}
-(void)touchesEnded:(NSSet*)touches withEvent:(UIEvent*)event
{
    [_decoder processTouches:touches withEvent:event]; //includedbypt
    UnitySendTouchesEnded(touches,event);
}

-(void)touchesCancelled:(NSSet*)touches withEvent:(UIEvent*)event
{
    [_decoder processTouches:touches withEvent:event]; //includedbypt
    UnitySendTouchesCancelled(touches,event);
}
-(void)touchesMoved:(NSSet*)touches withEvent:(UIEvent*)event
{
    [_decoder processTouches:touches withEvent:event]; //includedbypt
    UnitySendTouchesMoved(touches,event);
}
-(void)dealloc
{
    [super dealloc];
    [[PTEventManager sharedManager]removeObserver:self]; //includedbypt
    [_decoder release]; //includedbypt
}

-(void) layoutSubviews
{
    [super layoutSubviews];
    
}

@end

void _TD_EAGL_init( const char *database, const char *key, bool *isNativeHelpZone)
{
    
    _sdataBaseName = [NSString stringWithUTF8String:(database)];
    
    _skeyInit = [NSString stringWithUTF8String:(key)];
    
    NSLog(@"_TD_EAGL_init() db:%@ key:%@", _sdataBaseName, _skeyInit);
    
    /*
     CGFloat scale = [UIScreen mainScreen].scale;
     
     EAGLView* viever = [[EAGLView alloc] initWithFrame:CGRectMake((CGFloat)PosX/scale, (CGFloat)PosY/scale, (CGFloat)width/scale, (CGFloat)height/scale)];
     [viever setBackgroundColor:[UIColor redColor]];
     [viever setAlpha:0.2f];
     UIView *topSubview = [[[UIApplication sharedApplication].delegate.window subviews] lastObject];
     [viever setAutoresizesSubviews:YES];
     [topSubview addSubview:viever];
     */
    
    
    EAGLView* viever = [[EAGLView alloc] initWithFrame:[[UIScreen mainScreen]bounds]];
    
    [viever setAutoresizesSubviews:YES];
    
    UIView *mainview = ((UnityAppController*) [UIApplication sharedApplication].delegate).rootViewController.view;
    NSLog(@"Mainview %@", mainview);
    
    [mainview addSubview:viever];
    
    if( isNativeHelpZone ){
        
        CGSize detectionZoneRect = CGSizeMake([PTEventManager sharedManager ].coreDimensionInPoint.height,[PTEventManager sharedManager ].mediaDimensionInPoint.height);
        
        // ** uncomment to enable TouchcodeUI **
        // PTDetectionZone *detectionZone = [[PTDetectionZone alloc] initWithSize:detectionZoneRect withSuperFrame:viever.frame  andOrientation:TOUCHCODE_DETECTION_ORIENTATION_EAST andPositionOnBorderLandscape:500 andPositionOnBorderPortrait:600];
        
        // register a callback for successful detection
        // ** uncomment to enable TouchcodeUI **
        // [detectionZone addTouchCodeDetectionTarget:viever action:@selector(TCActionSuccesfulDetected:)];
        
        // ** uncomment to enable TouchcodeUI **
        // viever.helpLayer = [[PTHelpLayer alloc] initWithFrame:mainview.frame withDetectionZone:detectionZone withCardPreviewImage:nil withDetectionZoneStateView:nil];
        
        // define helpLayer with detectionZone
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
        {
            // ** uncomment to enable TouchcodeUI **
            // viever.helpLayer.orientationGuide = [[[PTOrientationGuide alloc] initWithFrame:CGRectMake(viever.helpLayer.detectionZoneStateView.frame.origin.x-60, viever.helpLayer.detectionZoneStateView.frame.origin.y, viever.helpLayer.detectionZoneStateView.frame.size.width, viever.helpLayer.detectionZoneStateView.frame.size.height)] autorelease];
            
        }
        else
        {
            
        }
        
        // ** uncomment to enable TouchcodeUI **
        // [mainview addSubview:viever.helpLayer];
    }
}



void _TouchDetected()
{
    
    PTTouchDecodeResult* result= [[PTTouchDecodeResult alloc] init];
    result.code_name = @"Hello world! 2015";
    [[EAGLView alloc] didUnsafeDecodeWithResult: result];
    
    
}

